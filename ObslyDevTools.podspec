Pod::Spec.new do |s|
  s.name     = 'ObslyDevTools'
  s.version  = '0.0.1'
  s.ios.deployment_target     = '10.3'
  s.license  = { :type => 'Commercial', :text => 'See https://gitlab.com/obsly/obsly.git' }
  s.summary  = 'Obsly:a mobile remote logger'
  s.description = 'mobile development easier with Obsly.'
  s.homepage = 'https://gitlab.com/obsly/obslydevtools'
  s.author   = { 'Obsly' => 'joaquinc@sfy.com' }
  s.requires_arc = true
  s.source   = {
    :git => 'https://gitlab.com/obsly/obslydevtools.git',
    :tag => s.version.to_s
  }
  s.swift_versions = ['5.1']
  s.frameworks = "Foundation", "SystemConfiguration", "Security", "MobileCoreServices"
  s.library = 'c++'
  s.dependency 'ObslySDK', '>= 1.4.0'
  s.vendored_frameworks = 'ObslyDevToolsSDK.xcframework'
  s.cocoapods_version = '>= 1.10.0'
  s.pod_target_xcconfig = { 'VALID_ARCHS' => 'arm64 arm64e armv7 armv7s x86_64' }
  
end