//
//  ObslyToolsSDK.h
//  ObslyToolsSDK
//
//  Created by Pedro Angel Oliver on 5/10/22.
//

#import <Foundation/Foundation.h>

//! Project version number for ObslyToolsSDK.
FOUNDATION_EXPORT double ObslyToolsSDKVersionNumber;

//! Project version string for ObslyToolsSDK.
FOUNDATION_EXPORT const unsigned char ObslyToolsSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ObslyToolsSDK/PublicHeader.h>


